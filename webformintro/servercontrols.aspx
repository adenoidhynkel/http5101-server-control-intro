﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="servercontrols.aspx.cs" Inherits="webformintro.servercontrols" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Pizza Order</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <p>Pizza Order</p>
            <asp:TextBox runat="server" ID="clientName" placeholder="Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Name" ControlToValidate="clientName" ID="validatorName"></asp:RequiredFieldValidator>
            <br />
            <asp:TextBox runat="server" ID="clientPhone" placeholder="Phone"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="clientPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
            <br />
            <asp:CompareValidator runat="server" ControlToValidate="clientPhone" Type="String" Operator="NotEqual" ValueToCompare="9051231234" ErrorMessage="This is an invalid phone number"></asp:CompareValidator>
            <br />
            <asp:TextBox runat="server" ID="clientEmail" placeholder="Email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="clientEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br />
            <asp:TextBox runat="server" ID="deliveryAddr" placeholder="Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="deliveryAddr" ErrorMessage="Please enter an address"></asp:RequiredFieldValidator>
            <br />
            <asp:TextBox runat="server" ID="deliveryTime" placeholder="Delivery Time (24 hour format)"></asp:TextBox>
            <asp:RangeValidator EnableClientScript="false" ID="hourscontrol_1" runat="server" ControlToValidate="deliveryTime" MinimumValue="1100" Type="Integer" MaximumValue="2200" ErrorMessage="Our hours are between 11am and 10pm"></asp:RangeValidator>
            <br />
            <asp:TextBox runat="server" ID="pizzaSlices" placeholder="Number of Slices"></asp:TextBox>
            <asp:RangeValidator runat="server" ControlToValidate="pizzaSlices" Type="Integer" MinimumValue="2" MaximumValue="12" ErrorMessage="Enter a valid number of slices (between 2 and 12)"></asp:RangeValidator>
            <br />
            <asp:DropDownList runat="server" ID="pizzaSize">
                <asp:ListItem Value="S" Text="Small"></asp:ListItem>
                <asp:ListItem Value="M" Text="Medium"></asp:ListItem>
                <asp:ListItem Value="L" Text="Large"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:RadioButton runat="server" Text="Delivery" GroupName="via"/>
            <asp:RadioButton runat="server" Text="Pick-Up" GroupName="via"/>
            <br />
            <div id="toppings" runat="server">
            <asp:CheckBox runat="server" ID="pizzaTopping1" Text="Mushrooms" />
            <asp:CheckBox runat="server" ID="pizzaTopping2" Text="Pineapple" />
            <asp:CheckBox runat="server" ID="pizzaTopping3" Text="Pepperoni" />
            </div>
            <br />
            <asp:Button runat="server" ID="myButton" OnClick="MakePizza" Text="Submit"/>
            <br />
            <div runat="server" ID="res"></div>
            
        </div>
    </form>
</body>
</html>
