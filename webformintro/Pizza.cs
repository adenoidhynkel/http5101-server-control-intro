﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webformintro
{
    public class Pizza
    {
        /*
         A list of strings to represent the toppings. Example "Pepperoni", "Cheese", "Olives", etc.
         */
        public List<string> toppings;
        public Pizza(List<string> t)
        {
            toppings = t;
        }
    }
}