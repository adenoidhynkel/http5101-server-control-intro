﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace webformintro
{
    public partial class servercontrols : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void MakePizza(object sender, EventArgs e)
        {
            Customer bestcustomer = new Customer();
            bestcustomer.CustomerName = clientName.Text;
            bestcustomer.CustomerPhone = clientPhone.Text;
            bestcustomer.CustomerEmail = clientEmail.Text;

            List<string> mytoppings = new List<string>();

            foreach(Control control in toppings.Controls)
            {
                if(control.GetType()==typeof(CheckBox))
                {
                    CheckBox topping = (CheckBox)control;
                    if (topping.Checked)
                    {
                        mytoppings.Add(topping.Text);
                    }
                   
                }
            }

            Pizza goodPizza = new Pizza(mytoppings);
            
            foreach(string topping in goodPizza.toppings)
            {
                //res.InnerText += topping;

            }
            
        }
    }
}